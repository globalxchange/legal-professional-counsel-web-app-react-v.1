var mongoose = require('mongoose');
var UserSchema = require('../module/register.module');

UserSchema.statics = {
    create:function(data, cb){
        var user = new this(data);
        user.save(cb);   
    },
    get: function(query, cb){
        this.find(query, cb);
    },
    getByName: function(query, cb){
        this.find(query, cb);
    },
    update: function(query, updateData, cb){
        this.findOneandUpdate(query, {$set:updateData},{new:true}, cb)
    },
    delete: function(query, cb){
        this.findOneandDelete(query,cb);
    }
}

var UserModel = mongoose.model('users',UserSchema);
module.exports = UserModel;