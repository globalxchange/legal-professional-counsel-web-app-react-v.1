var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    app_code: {
        type: String,
        required: true
    },
    profile_id: {
        type: String,
        required: true
    },
    userType: {
        type: String,
        required: false
    },
    CorporateProfile: {
        type: Array,
        required: false,
        CorporateName: {
            type: String,
            required: false
        }
    },
    LegalFirm:{
        type:Array,
        required:false,
        LegalProfessional:{
            type: String,
            required: false
        }
    }
}, {
    timestamps: true
});

module.exports = UserSchema;